require 'compass/import-once/activate'
require 'compass/import-once/activate'
# Require any additional compass plugins here.


http_path = "/"
css_dir = "stylesheets"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

output_style = :compact

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

line_comments = false

preferred_syntax = :scss
